package com.example.myapplication_tp2_android;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.tp2.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {


    ListView listView;
    Cursor cursor;
    SimpleCursorAdapter adapter;
    WineDbHelper WineDbHelper;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar); // toolbar
        setSupportActionBar(toolbar);

        WineDbHelper = new WineDbHelper(getApplicationContext());
        // remplir la bd
        WineDbHelper.populate();


        cursor = WineDbHelper.fetchAllWines();
        cursor.moveToFirst();





        adapter = new SimpleCursorAdapter(

                this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[]{

                        WineDbHelper.COLUMN_NAME,
                        WineDbHelper.COLUMN_WINE_REGION
                },                              //Tableau de colonnes
                new int[]{android.R.id.text1, android.R.id.text2

                },                             // lier les views aux collone
                0);

        //supprimer un Wine
        listView = (ListView) findViewById(R.id.listView);
        registerForContextMenu(listView);
        listView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

                menu.add(Menu.NONE, 1, Menu.NONE, "supprimer");

            }
        });



        // Cliquer sur un item d'une listview pour lancer wineactivité

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {

                Intent myIntent = new Intent(v.getContext(), WineActivity.class);
                Cursor cursor = (Cursor) listView.getItemAtPosition(position);
                Wine Selected = WineDbHelper.cursorToWine(cursor);
                Log.d("Ouvrire activity wine", Selected.toString());
                myIntent.putExtra("idir", Selected);
                startActivity(myIntent);
            }
        });



        //le button ajouter (+)
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), WineActivity.class);
                myIntent.putExtra("idir", (Bundle) null);
                startActivity(myIntent);

            }
        });
    }


    // update  notre liste
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        cursor.moveToPosition(info.position);
        WineDbHelper.deleteWine(cursor);
        cursor = WineDbHelper.fetchAllWines();
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();

        return true;
    }


    //pour mettre a jour l'activite avec la methode onResume()
    @Override
    protected void onResume() {
        super.onResume();
        cursor = WineDbHelper.fetchAllWines();
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();

    }


}

